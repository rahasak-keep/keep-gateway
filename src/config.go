package main

import (
	"os"
)

type Config struct {
	serviceName string
	dotKeys     string
	idRsa       string
	idRsaPub    string
	authRsaPub  string
	dotLogs     string
}

type FeatureToggleConfig struct {
	enableVerifyToken string
}

type ApiConfig struct {
	deviceApi       string
	accountApi      string
	keepApi         string
	offerApi        string
	notificationApi string
}

type MongoConfig struct {
	mongoHost string
	mongoPort string
	mongoDb   string
	username  string
	password  string
	keyColl   string
	senzColl  string
}

var config = Config{
	serviceName: getEnv("SERVICE_NAME", "lekana.gateway"),
	dotKeys:     getEnv("DOT_KEYS", ".keys"),
	idRsa:       getEnv("ID_RSA", ".keys/id_rsa"),
	idRsaPub:    getEnv("ID_RSA_PUB", ".keys/id_rsa.pub"),
	authRsaPub:  getEnv("AUTH_RSA_PUB", "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCgAqBFZxpTfXqAFB4oQI+vvO/MCblRzImOGIX3SuZaq959HHybfXwnSNrlO35SCQglA0tJw5HwnhrHdOMINWFGmSRa9wEhYv49HiMwlJkFR5rZIqQk/141SxFolyVBZPLMP/wOvYnZOEUygZ9lSexoKQqIhh+al4lToeDWEL2u/wIDAQAB"),
	dotLogs:     getEnv("DOT_LOGS", ".logs"),
}

var featureToggleConfig = FeatureToggleConfig{
	enableVerifyToken: getEnv("ENABLE_VERIFY_TOKEN", "no"),
}

var apiConfig = ApiConfig{
	accountApi: getEnv("ACCOUNT_API", "http://dev.localhost:8761/api/accounts"),
	keepApi:    getEnv("KEEP_API", "http://dev.localhost:8761/api/keeps"),
	offerApi:   getEnv("OFFER_API", "http://dev.localhost:8761/api/offers"),
}

var mongoConfig = MongoConfig{
	mongoHost: getEnv("MONGO_HOST", "dev.localhost"),
	mongoPort: getEnv("MONGO_PORT", "27017"),
	mongoDb:   getEnv("MONGO_DB", "senz"),
	username:  getEnv("MONGO_USER", "senz"),
	password:  getEnv("MONGO_PASS", "senz"),
	keyColl:   "keys",
	senzColl:  "senzes",
}

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}

	return fallback
}
