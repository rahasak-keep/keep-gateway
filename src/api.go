package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

func initApi() {
	// router
	r := mux.NewRouter()
	r.HandleFunc("/api/accounts", apiAccount).Methods("POST")
	r.HandleFunc("/api/keeps", apiKeep).Methods("POST")
	r.HandleFunc("/api/offers", apiOffer).Methods("POST")

	// handlers
	origins := handlers.AllowedOrigins([]string{"*"})
	headers := handlers.AllowedHeaders([]string{"Authorization", "Bearer", "Content-Type"})
	methods := handlers.AllowedMethods([]string{"GET", "POST", "OPTIONS"})

	// start server with CORS
	err := http.ListenAndServe(":7654", handlers.CORS(origins, headers, methods)(r))
	if err != nil {
		log.Printf("ERROR: fail init http server, %s", err.Error)
		os.Exit(1)
	}
}

func apiAccount(w http.ResponseWriter, r *http.Request) {
	// read body
	data, _ := ioutil.ReadAll(r.Body)
	defer r.Body.Close()

	log.Printf("INFO: request account contract %s", string(data))

	// unmarshal json
	jsonMap := make(map[string]interface{})
	err := json.Unmarshal(data, &jsonMap)
	if err != nil {
		log.Printf("ERROR: fail to unmarshla json, %s", err.Error())
		response(w, "Invalid request json", 400)
		return
	}

	msgTyp := fmt.Sprintf("%s", jsonMap["messageType"])
	if msgTyp == "register" || msgTyp == "activate" || msgTyp == "connect" || msgTyp == "changePassword" {
		// auth requests
		// call blockchian to handle account auth
		resp, status := post(data, apiConfig.accountApi)
		response(w, resp, status)
		return
	}

	if msgTyp == "create" || msgTyp == "update" || msgTyp == "addRole" || msgTyp == "removeRole" || msgTyp == "search" || msgTyp == "delete" {
		// handle auth if enable
		if featureToggleConfig.enableVerifyToken == "yes" {
			tokenStr := r.Header.Get("Bearer")
			err = verifyToken(tokenStr)
			if err != nil {
				// unauthorized
				log.Printf("ERROR: fail to verify auth token, %s", err.Error())
				response(w, "Fail to verify Bearer", 401)
				return
			}
		}

		// call blockchian to handle account
		resp, status := post(data, apiConfig.accountApi)
		response(w, resp, status)
	}
}

func apiKeep(w http.ResponseWriter, r *http.Request) {
	// read body
	data, _ := ioutil.ReadAll(r.Body)
	defer r.Body.Close()

	log.Printf("INFO: request keep contract %s", string(data))

	// handle auth if enable
	if featureToggleConfig.enableVerifyToken == "yes" {
		tokenStr := r.Header.Get("Bearer")
		err := verifyToken(tokenStr)
		if err != nil {
			// unauthorized
			log.Printf("ERROR: fail to verify auth token, %s", err.Error())
			response(w, "Fail to verify Bearer", 401)
			return
		}
	}

	// call blockchain to handle keep
	resp, status := post(data, apiConfig.keepApi)
	response(w, resp, status)
}

func apiOffer(w http.ResponseWriter, r *http.Request) {
	// read body
	data, _ := ioutil.ReadAll(r.Body)
	defer r.Body.Close()

	log.Printf("INFO: request offer contract %s", string(data))

	// handle auth if enable
	if featureToggleConfig.enableVerifyToken == "yes" {
		tokenStr := r.Header.Get("Bearer")
		err := verifyToken(tokenStr)
		if err != nil {
			// unauthorized
			log.Printf("ERROR: fail to verify auth token, %s", err.Error())
			response(w, "Fail to verify Bearer", 401)
			return
		}
	}

	// call blockchain to handle keep
	resp, status := post(data, apiConfig.offerApi)
	response(w, resp, status)
}

func response(w http.ResponseWriter, resp string, status int) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	io.WriteString(w, resp)
}
